# 1.0.0 (2019-10-30)


### Bug Fixes

* Add initial installer files ([7973943](https://gitlab.com/dreamer-labs/bashellite/bashellite-installer/commit/7973943))


### Features

* Add container and CI ([1d71096](https://gitlab.com/dreamer-labs/bashellite/bashellite-installer/commit/1d71096))
